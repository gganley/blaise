use crate::lexer::{Lexer, Token};
use crate::parser::ast;
use crate::parser::parselet::{Infix, Prefix};

pub enum Precedence {
    Call = 6,
    UnaryNum = 5,
    Product = 4,
    Sum = 3,
    Comp = 2,
    Terms = 1,
}

pub struct Parser<'a> {
    pub lexer: Lexer<'a>,
}

impl<'a> Parser<'a> {
    pub fn new(lexer: Lexer<'a>) -> Self {
        Parser { lexer }
    }

    fn get_prefix_parselet(token: &Token) -> Result<Prefix, String> {
        match token {
            Token::Int(_) | Token::Bool(_) => Ok(Prefix::Literal),
            Token::Plus | Token::Minus => Ok(Prefix::UnaryOp(Precedence::UnaryNum as u32)),
            Token::Not => Ok(Prefix::UnaryOp(Precedence::Terms as u32)),
            Token::LParen => Ok(Prefix::Grouping),
            Token::ID(_) => Ok(Prefix::Variable),
            _ => Err("Error".to_string()),
        }
    }

    fn get_infix(token: &Token) -> Result<Infix, String> {
        match token {
            Token::Plus | Token::Minus => Ok(Infix::BinOp(Precedence::Sum as u32)),
            Token::Mult | Token::Div => Ok(Infix::BinOp(Precedence::Product as u32)),
            Token::And | Token::Or => Ok(Infix::BinOp(Precedence::Terms as u32)),
            Token::Lt | Token::LtEq | Token::Gt | Token::GtEq | Token::Eq | Token::NotEq => {
                Ok(Infix::BinOp(Precedence::Comp as u32))
            }
            Token::LParen => Ok(Infix::FnCall(Precedence::Call as u32)),
            _ => Err("Error".to_string()),
        }
    }

    fn get_next_precedence(&mut self) -> u32 {
        match self.lexer.peek() {
            Some(token) => match Parser::get_infix(token) {
                Ok(parselet) => parselet.get_precedence(),
                Err(_) => 0,
            },
            None => 0,
        }
    }
    pub fn program(&mut self) -> Result<ast::Program, String> {
        match self.lexer.next() {
            Some(Token::Program) => {}
            _ => return Err("Error".to_string()),
        };

        let variable = self.var()?;

        match self.lexer.next() {
            Some(Token::SemiColon) => {}
            _ => return Err("Error".to_string()),
        };

        let block = self.block()?;

        match self.lexer.next() {
            Some(Token::Dot) => {}
            _ => return Err("Error".to_string()),
        };

        Ok(ast::Program(variable, block))
    }

    pub fn block(&mut self) -> Result<ast::Block, String> {
        match self.stmt_block() {
            Ok(ast::StmtBlock(stmts)) => Ok(ast::Block(self.decls()?, stmts)),
            _ => Err("Error".to_string()),
        }
    }

    pub fn decls(&mut self) -> Result<Vec<ast::Decl>, String> {
        let mut decls = Vec::new();

        if let Some(&Token::Var) = self.lexer.peek() {
            self.lexer.next();

            let mut variable_decls = Vec::new();

            while let Some(&Token::ID(_)) = self.lexer.peek() {
                variable_decls.push(self.var_decl()?);
            }

            if variable_decls.len() > 0 {
                decls.push(ast::Decl::VarDecl(variable_decls));
            } else {
                return Err("No variables declared".to_string());
            }
        }

        let mut proc_decls = Vec::new();
        let mut fn_decls = Vec::new();

        loop {
            match self.lexer.peek() {
                Some(Token::Procedure) => proc_decls.push(self.proc_decl()?),
                Some(Token::Function) => fn_decls.push(self.fn_decl()?),
                _ => break,
            }
        }

        decls.push(ast::Decl::ProcDecl(proc_decls));
        decls.push(ast::Decl::FnDecl(fn_decls));

        if decls.is_empty() {
            decls.push(ast::Decl::Epsilon);
        }

        Ok(decls)
    }

    pub fn var_decl(&mut self) -> Result<ast::VarDecl, String> {
        let mut ids = Vec::new();

        match self.lexer.next() {
            Some(Token::ID(name)) => {
                ids.push(name);
            }
            _ => return Err("No variable name".to_string()),
        };

        while let Some(Token::Comma) = self.lexer.peek() {
            self.lexer.next();

            match self.lexer.next() {
                Some(Token::ID(name)) => {
                    ids.push(name);
                }
                _ => return Err("No variable name".to_string()),
            };
        }

        match self.lexer.next() {
            Some(Token::Colon) => match (self.type_type()?, self.lexer.next()) {
                (type_type, Some(Token::SemiColon)) => Ok(ast::VarDecl(ids, type_type)),
                (type_type, Some(a)) => {
                    Err(format!("No semicolon after {:?}, found {:?}", type_type, a))
                }
                _ => Err(format!("Everything is broken")),
            },
            _ => Err(format!("Everything is borked")),
        }
    }

    pub fn type_type(&mut self) -> Result<ast::Type, String> {
        match self.lexer.next() {
            Some(Token::IntT) => Ok(ast::Type::Int),
            Some(Token::BoolT) => Ok(ast::Type::Bool),
            _ => Err(String::from(format!("This is not a type my dude"))),
        }
    }

    pub fn proc_decl(&mut self) -> Result<ast::ProcDecl, String> {
        let name = match (self.lexer.next(), self.lexer.next()) {
            (Some(Token::Procedure), Some(Token::ID(name))) => name,
            _ => return Err(format!("Procedure plz?")),
        };
        let parameters = match self.lexer.peek() {
            Some(Token::LParen) => {
                self.lexer.next();

                match (self.parameter_list()?, self.lexer.next()) {
                    (parameter_list, Some(Token::RParen)) => parameter_list,
                    _ => return Err(format!("Where the hell is my right paren")),
                }
            }
            _ => ast::ParameterList(Vec::new()),
        };

        match self.lexer.next() {
            Some(Token::SemiColon) => Ok(ast::ProcDecl(name, parameters, self.block()?)),
            _ => Err(format!("Where be my semicolon")),
        }
    }

    pub fn fn_decl(&mut self) -> Result<ast::FnDecl, String> {
        let name = match (self.lexer.next(), self.lexer.next()) {
            (Some(Token::Function), Some(Token::ID(name))) => name,
            _ => return Err(format!("I was expecting something else")),
        };

        let parameters = match self.lexer.next() {
            Some(Token::LParen) => match (self.parameter_list()?, self.lexer.next()) {
                (parameter_list, Some(Token::RParen)) => parameter_list,
                _ => return Err(format!("What is this! I demand a right paren!")),
            },
            _ => return Err(format!("You couldn't even get the right paren to match?")),
        };

        let return_type = match self.lexer.next() {
            Some(Token::Colon) => self.type_type()?,
            _ => return Err(format!("No colon, 1 star")),
        };

        let block = match self.lexer.next() {
            Some(Token::SemiColon) => self.block()?,
            _ => return Err(format!("No semi, no block")),
        };

        Ok(ast::FnDecl(name, parameters, return_type, block))
    }

    pub fn parameter_list(&mut self) -> Result<ast::ParameterList, String> {
        let mut parameters = Vec::new();

        if let Some(Token::ID(_)) = self.lexer.peek() {
            loop {
                parameters.push(self.parameters()?);

                match self.lexer.peek() {
                    Some(Token::SemiColon) => self.lexer.next(),
                    _ => break,
                };
            }
        }

        Ok(ast::ParameterList(parameters))
    }

    pub fn parameters(&mut self) -> Result<ast::Parameter, String> {
        let mut ids = Vec::new();

        match self.lexer.next() {
            Some(Token::ID(name)) => {
                ids.push(name);
            }
            _ => return Err(format!("No name")),
        }

        while let Some(Token::Comma) = self.lexer.peek() {
            let _thrown_out_comma = self.lexer.next();

            match self.lexer.next() {
                Some(Token::ID(name)) => {
                    ids.push(name);
                }
                _ => return Err(format!("Just give me a god damn name!")),
            }
        }

        match self.lexer.next() {
            Some(Token::Colon) => Ok(ast::Parameter(ids, self.type_type()?)),
            _ => Err(format!("I need a colon")),
        }
    }

    pub fn stmt_block(&mut self) -> Result<ast::StmtBlock, String> {
        match self.lexer.next() {
            Some(Token::Begin) => {}
            _ => {
                return Err(format!(
                    "Without a beginning how can there be comile said Schuenzu"
                ))
            }
        }

        let mut stmts = Vec::new();

        while let Some(token) = self.lexer.peek() {
            match token {
                Token::End => {
                    let _eat_end = self.lexer.next();
                    break;
                }
                _ => stmts.push(self.stmt()?),
            }
        }

        Ok(ast::StmtBlock(stmts))
    }

    pub fn stmt(&mut self) -> Result<ast::Stmt, String> {
        match self.lexer.peek() {
            Some(Token::Begin) => Ok(ast::Stmt::StmtBlock(self.stmt_block()?)),
            Some(Token::ID(_)) => match self.lexer.peek_ahead(1) {
                Some(Token::LParen) => Ok(ast::Stmt::FnCall(self.fn_call()?)),
                Some(Token::Assign) => Ok(ast::Stmt::Assignment(self.assignment_stmt()?)),
                _ => Err(format!(
                    "There was an ID but no assign or paren, what gives?"
                )),
            },
            Some(Token::If) => Ok(ast::Stmt::IfStmt(self.if_stmt()?)),
            _ => Err(format!("For real though what happend here")),
        }
    }

    pub fn if_stmt(&mut self) -> Result<ast::IfStmt, String> {
        let (if_expr, if_block) = match self.lexer.next() {
            Some(Token::If) => match (self.expr(None)?, self.lexer.next()) {
                (expr, Some(Token::Then)) => (expr, self.stmt_block()?),
                _ => return Err(format!("What")),
            },
            _ => return Err(format!("Writting good error messages is hard")),
        };

        match self.lexer.peek() {
            Some(Token::Else) => {
                self.lexer.next(); // eat the 'else' token
                match self.lexer.peek() {
                    Some(Token::If) => Ok(ast::IfStmt::IfElseIf(
                        if_expr,
                        if_block,
                        Box::new(self.if_stmt()?),
                    )),
                    _ => Ok(ast::IfStmt::IfElse(if_expr, if_block, self.stmt_block()?)),
                }
            }
            _ => Ok(ast::IfStmt::If(if_expr, if_block)),
        }
    }

    pub fn assignment_stmt(&mut self) -> Result<ast::Assignment, String> {
        match (self.var()?, self.lexer.next()) {
            (var, Some(Token::Assign)) => match (self.expr(None)?, self.lexer.next()) {
                (expr, Some(Token::SemiColon)) => Ok(ast::Assignment(var, expr)),
                _ => Err(format!("Die imperial scum!")),
            },
            _ => Err(format!("Death to the storm cloaks!")),
        }
    }

    pub fn var(&mut self) -> Result<ast::Var, String> {
        match self.lexer.next() {
            Some(Token::ID(id)) => Ok(ast::Var(id)),
            _ => Err(format!("How can you have a variable without an ID!")),
        }
    }

    pub fn fn_call(&mut self) -> Result<ast::FnCall, String> {
        let function_id = match (self.var()?, self.lexer.next()) {
            (variable, Some(Token::LParen)) => variable,
            _ => return Err(format!("Function call?")),
        };

        match (self.fn_parameters()?, self.lexer.next(), self.lexer.next()) {
            (call_parameters, Some(Token::RParen), Some(Token::SemiColon)) => {
                Ok(ast::FnCall(function_id, call_parameters))
            }
            _ => Err(format!("Something")),
        }
    }

    pub fn fn_parameters(&mut self) -> Result<ast::FnParameters, String> {
        if let Some(&Token::RParen) = self.lexer.peek() {
            Ok(ast::FnParameters(Vec::new()))
        } else {
            let mut parameters = Vec::new();
            parameters.push(self.expr(None)?);

            while let Some(&Token::Comma) = self.lexer.peek() {
                let _eat_the_comma = self.lexer.next();
                parameters.push(self.expr(None)?);
            }

            Ok(ast::FnParameters(parameters))
        }
    }

    pub fn expr(&mut self, precedence: Option<u32>) -> Result<ast::Expr, String> {
        let precedence = precedence.unwrap_or(0);
        let token = self.lexer.next().ok_or(String::from(
            "Expression Parse Error: Expected a token, found none",
        ))?;
        let parselet = Parser::get_prefix_parselet(&token)?;

        let mut left = parselet.parse(self, &token)?;

        while precedence < self.get_next_precedence() {
            let token = self.lexer.next().ok_or(String::from(
                "Expression Parse Error: Expected a token, found none",
            ))?;
            let parselet = Parser::get_infix(&token)?;

            left = parselet.parse(self, &left, &token)?;
        }

        Ok(left)
    }
    pub fn parse(&mut self) -> Result<ast::Program, String> {
        let program = self.program()?;

        match self.lexer.next() {
            Some(Token::EOF) => Ok(()),
            Some(token) => Err(String::from(format!(
                "Parse Error: Expected {:?} token, found token {:?}",
                Token::EOF,
                token
            ))),
            None => Err(String::from(format!(
                "Parse Error: Expected {:?} token, found None",
                Token::EOF
            ))),
        }?;

        Ok(program)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::lexer::Source;

    fn get_parser(text: &str) -> Parser {
        let source = Source::new(text);
        let lexer = Lexer::new(source);
        let parser = Parser::new(lexer);

        return parser;
    }

    #[test]
    fn declarations() {
        let mut parser = get_parser("var test_var: integer; function test_func(): integer; begin end procedure test_proc; begin end");
        match parser.decls() {
            Ok(declarations) => {
                assert_eq!(declarations.len(), 3);
            }
            Err(e) => panic!(e),
        };
    }

    #[test]
    fn variable_declaration() {
        let mut parser = get_parser("foo, bar, baz : integer;");
        match parser.var_decl() {
            Ok(ast::VarDecl(names, _)) => assert_eq!(
                names,
                vec![
                    String::from("foo"),
                    String::from("bar"),
                    String::from("baz")
                ]
            ),
            Err(e) => panic!(e),
        }
    }

    #[test]
    fn type_spec() {
        let mut parser = get_parser("integer boolean");

        assert_eq!(parser.type_type(), Ok(ast::Type::Int));
        assert_eq!(parser.type_type(), Ok(ast::Type::Bool));
    }

    #[test]
    fn procedure_declaration() {
        let mut parser = get_parser("procedure test(); begin end");
        match parser.proc_decl() {
            Ok(ast::ProcDecl(name, _, _)) => assert_eq!(name, String::from("test")),
            Err(e) => panic!(e),
        }
    }

    #[test]
    fn function_declaration() {
        let mut parser = get_parser("function test(): integer; begin end");
        match parser.fn_decl() {
            Ok(ast::FnDecl(name, _, _, _)) => assert_eq!(name, String::from("test")),
            Err(e) => panic!(e),
        }
    }

    #[test]
    fn formal_parameters() {
        let mut parser = get_parser("a, b, c: integer");
        match parser.fn_parameters() {
            Ok(ast::FnParameters(names)) => assert_eq!(names.len(), 3),
            Err(e) => panic!(e),
        };
    }

}
