#[derive(Clone, Debug, PartialEq)]
pub struct Program(pub Var, pub Block);
#[derive(Clone, Debug, PartialEq)]
pub struct Block(pub Vec<Decl>, pub Vec<Stmt>);
#[derive(Clone, Debug, PartialEq)]
pub enum Decl {
    ProcDecl(Vec<ProcDecl>),
    FnDecl(Vec<FnDecl>),
    VarDecl(Vec<VarDecl>),
    Epsilon,
}
#[derive(Clone, Debug, PartialEq)]
pub struct ProcDecl(pub String, pub ParameterList, pub Block);
#[derive(Clone, Debug, PartialEq)]
pub struct FnDecl(pub String, pub ParameterList, pub Type, pub Block);
#[derive(Clone, Debug, PartialEq)]
pub struct ParameterList(pub Vec<Parameter>);
#[derive(Clone, Debug, PartialEq)]
pub struct Parameter(pub Vec<String>, pub Type);
#[derive(Clone, Debug, PartialEq)]
pub struct VarDecl(pub Vec<String>, pub Type);
#[derive(Clone, Debug, PartialEq)]
pub enum Type {
    Int,
    Bool,
}

#[derive(Clone, Debug, PartialEq)]
pub struct StmtBlock(pub Vec<Stmt>);
#[derive(Clone, Debug, PartialEq)]
pub enum Stmt {
    StmtBlock(StmtBlock),
    Assignment(Assignment),
    IfStmt(IfStmt),
    FnCall(FnCall),
}

#[derive(Clone, Debug, PartialEq)]
pub enum IfStmt {
    If(Expr, StmtBlock),
    IfElse(Expr, StmtBlock, StmtBlock),
    IfElseIf(Expr, StmtBlock, Box<IfStmt>),
}

#[derive(Clone, Debug, PartialEq)]
pub struct Var(pub String);

#[derive(Clone, Debug, PartialEq)]
pub struct FnCall(pub Var, pub FnParameters);

#[derive(Clone, Debug, PartialEq)]
pub struct FnParameters(pub Vec<Expr>);

#[derive(Clone, Debug, PartialEq)]
pub enum Expr {
    Group(Box<Group>),
    UnaryOp(Box<UnaryOpExpr>),
    BinOp(Box<BinOpExpr>),
    FnCall(FnCall),
    Literal(Literal),
    Var(Var),
}

#[derive(Clone, Debug, PartialEq)]
pub struct Group(pub Expr);

#[derive(Clone, Debug, PartialEq)]
pub struct Assignment(pub Var, pub Expr);

#[derive(Clone, Debug, PartialEq)]
pub struct UnaryOpExpr(pub UnaryOp, pub Expr);

#[derive(Clone, Debug, PartialEq)]
pub enum UnaryOp {
    Plus,
    Minus,
    Not,
}

#[derive(Debug, Clone, PartialEq)]
pub struct BinOpExpr(pub Expr, pub BinOp, pub Expr);

#[derive(Debug, Clone, PartialEq)]
pub enum BinOp {
    Plus,
    Minus,
    Mult,
    Div,
    And,
    Or,
    Lt,
    LtEq,
    Gt,
    GtEq,
    Eq,
    NotEq,
}

#[derive(Clone, Debug, PartialEq)]
pub enum Literal {
    Int(i32),
    Boolean(bool),
}
