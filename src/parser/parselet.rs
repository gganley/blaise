use crate::lexer::Token;
use crate::parser::ast;
use crate::parser::Parser;

pub enum Prefix {
    Literal,
    Grouping,
    Variable,
    UnaryOp(u32),
}
pub enum Infix {
    FnCall(u32),
    BinOp(u32),
}

#[allow(unused_variables)]
impl Prefix {
    pub fn parse(&self, parser: &mut Parser, token: &Token) -> Result<ast::Expr, String> {
        match self {
            Prefix::Literal => Ok(ast::Expr::Literal(self.literal(parser, token)?)),
            Prefix::Grouping => Ok(ast::Expr::Group(Box::new(self.group(parser, token)?))),
            Prefix::Variable => Ok(ast::Expr::Var(self.variable(parser, token)?)),
            Prefix::UnaryOp(_) => Ok(ast::Expr::UnaryOp(Box::new(self.unary_op(parser, token)?))),
        }
    }

    pub fn get_precedence(&self) -> u32 {
        match self {
            Prefix::UnaryOp(precedence) => *precedence,
            _ => 0,
        }
    }

    fn literal(&self, parser: &mut Parser, token: &Token) -> Result<ast::Literal, String> {
        match token {
            Token::Int(i) => Ok(ast::Literal::Int(*i)),
            Token::Bool(b) => Ok(ast::Literal::Boolean(*b)),
            _ => Err("Couldnt convert literal".to_string()),
        }
    }

    fn group(&self, parser: &mut Parser, token: &Token) -> Result<ast::Group, String> {
        match token {
            Token::LParen => match (parser.expr(None)?, parser.lexer.next()) {
                (expr, Some(Token::RParen)) => Ok(ast::Group(expr)),
                _ => Err("Couldn't find rparen".to_string()),
            },
            _ => Err("No LParen".to_string()),
        }
    }

    fn variable(&self, parser: &mut Parser, token: &Token) -> Result<ast::Var, String> {
        match token {
            Token::ID(name) => Ok(ast::Var(name.clone())),
            _ => Err("error".to_string()),
        }
    }

    fn unary_op(&self, parser: &mut Parser, token: &Token) -> Result<ast::UnaryOpExpr, String> {
        let operator = match token {
            Token::Plus => ast::UnaryOp::Plus,
            Token::Minus => ast::UnaryOp::Minus,
            Token::Not => ast::UnaryOp::Not,
            _ => return Err("error".to_string()),
        };

        Ok(ast::UnaryOpExpr(
            operator,
            parser.expr(Some(self.get_precedence()))?,
        ))
    }
}

#[allow(unused_variables)]
impl Infix {
    pub fn parse(
        &self,
        parser: &mut Parser,
        left: &ast::Expr,
        token: &Token,
    ) -> Result<ast::Expr, String> {
        match self {
            Infix::BinOp(_) => Ok(ast::Expr::BinOp(Box::new(
                self.binary_op(parser, left, token)?,
            ))),
            Infix::FnCall(_) => Ok(ast::Expr::FnCall(self.function_call(parser, left, token)?)),
        }
    }

    pub fn get_precedence(&self) -> u32 {
        match self {
            Infix::BinOp(precedence) | Infix::FnCall(precedence) => *precedence,
        }
    }

    fn binary_op(
        &self,
        parser: &mut Parser,
        left: &ast::Expr,
        token: &Token,
    ) -> Result<ast::BinOpExpr, String> {
        let operator = match token {
            Token::Plus => ast::BinOp::Plus,
            Token::Minus => ast::BinOp::Minus,
            Token::Mult => ast::BinOp::Mult,
            Token::Div => ast::BinOp::Div,
            Token::And => ast::BinOp::And,
            Token::Or => ast::BinOp::Or,
            Token::Lt => ast::BinOp::Lt,
            Token::LtEq => ast::BinOp::LtEq,
            Token::Gt => ast::BinOp::Gt,
            Token::GtEq => ast::BinOp::GtEq,
            Token::Eq => ast::BinOp::Eq,
            Token::NotEq => ast::BinOp::NotEq,
            _ => return Err("error".to_string()),
        };

        Ok(ast::BinOpExpr(
            left.clone(),
            operator,
            parser.expr(Some(self.get_precedence()))?,
        ))
    }

    fn function_call(
        &self,
        parser: &mut Parser,
        left: &ast::Expr,
        token: &Token,
    ) -> Result<ast::FnCall, String> {
        match (left, token) {
            (ast::Expr::Var(name), Token::LParen) => {
                match (parser.fn_parameters()?, parser.lexer.next()) {
                    (parameters, Some(Token::RParen)) => Ok(ast::FnCall(name.clone(), parameters)),
                    _ => Err("error".to_string()),
                }
            }
            _ => Err("Error".to_string()),
        }
    }
}
